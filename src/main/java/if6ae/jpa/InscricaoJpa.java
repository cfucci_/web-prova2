/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if6ae.jpa;

import if6ae.entity.Inscricao;
import if6ae.entity.Inscricao_;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.CriteriaQuery;

/**
 *
 * @author a1175548
 */
public class InscricaoJpa {
    @PersistenceUnit
    private EntityManagerFactory factory;
    private EntityManager em;
    public InscricaoJpa(){
        em = factory.createEntityManager();
    }
    
    public Inscricao findByNumero(int num){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Inscricao> cq = cb.createQuery(Inscricao.class);
        Root<Inscricao> rt = cq.from(Inscricao.class);
        cq.where(cb.equal(rt.get(Inscricao_.numero), num));
        TypedQuery<Inscricao> q = em.createQuery(cq);
        return  q.getSingleResult();
    }
    public Inscricao findByCpf(int cpf){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Inscricao> cq = cb.createQuery(Inscricao.class);
        Root<Inscricao> rt = cq.from(Inscricao.class);
        cq.where(cb.equal(rt.get(Inscricao_.cpf), cpf));
        TypedQuery<Inscricao> q = em.createQuery(cq);
        return  q.getSingleResult();
    }
}
