/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if6ae.jpa;

import if6ae.entity.InscricaoMinicurso;
import if6ae.entity.InscricaoMinicurso_;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author a1175548
 */
public class InscricaoMinicursoJpa {
    @PersistenceUnit
    private EntityManagerFactory factory;
    private EntityManager em;
    
    public InscricaoMinicursoJpa(){
        em = factory.createEntityManager();
    }
    
    public List<InscricaoMinicurso> findInscricaoMinicursoByNumero(int num){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<InscricaoMinicurso> cq = cb.createQuery(InscricaoMinicurso.class);
        Root<InscricaoMinicurso> rt = cq.from(InscricaoMinicurso.class);
        cq.where(cb.equal(rt.get(InscricaoMinicurso_.inscricao), num));
        TypedQuery<InscricaoMinicurso> q = em.createQuery(cq);
        return q.getResultList();
    }
    
}
