/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if6ae.beans;

import if6ae.entity.InscricaoMinicurso;
import if6ae.jpa.InscricaoMinicursoJpa;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author a1175548
 */
@Named(value = "inscricaoBean")
@SessionScoped
public class InscricaoBean implements Serializable {
    
    private InscricaoMinicursoJpa inscricaoJpa = new InscricaoMinicursoJpa();
    private List<InscricaoMinicurso> listaInscricao;
    private int numero;
    /**
     * Creates a new instance of InscricaoBean
     */
    public InscricaoBean() {
    }
    
    public void getListaInscricao(int num){
        listaInscricao = inscricaoJpa.findInscricaoMinicursoByNumero(num);
    }
    
}
